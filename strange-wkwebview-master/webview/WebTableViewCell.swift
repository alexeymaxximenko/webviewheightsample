//
//  WebTableViewCell.swift
//  webview
//
//  Created by win on 9/17/16.
//  Copyright © 2016 Pawin Thepbanchaporn. All rights reserved.
//

import UIKit
import WebKit

class WebTableViewCell: UITableViewCell, WKNavigationDelegate {

    let webView = WKWebView()
    
    var tableView: UITableView!
    
    var height: CGFloat = 1200.0
    
    var heightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.allowsLinkPreview = true
        
        contentView.addSubview(webView)
        
        webView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        webView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure() {
        webView.navigationDelegate = self
        webView.scrollView.isScrollEnabled = false

//        let url = URL(string: "https://app.getunipress.com/data.html")!
//        let request = URLRequest(url: url)
//        webView.load(request)
        
        if let url = Bundle.main.url(forResource: "html",
                                     withExtension: "html") {
            if let htmlData = try? Data(contentsOf: url) {
                let baseURL = URL(fileURLWithPath: Bundle.main.bundlePath)
                webView.load(htmlData, mimeType: "text/html",
                             characterEncodingName: "UTF-8",
                             baseURL: baseURL)
            }
        }
        
//        var path = Bundle.main.path(forResource: "html", ofType: "html")
//        var url = URL(fileURLWithPath: path!)
//        var url2 = url.deletingLastPathComponent()
//        let request = URLRequest(url: url)
//        webView.load(request)
//        webView.loadFileURL(url, allowingReadAccessTo: URL(string: "file:///")!)
        
        
        /*
        webView.loadHTMLString("""
        <html>
            <head>
                <meta charset="UTF-8">
                    <meta name="referrer" content="none">

                        </head>
            <body>
                
                <script>window.alert = function() {};</script>
                
                <div class="imagebox image-sub"><div class="img_resizable"><img src="https://www.ringtv.com/wp-content/uploads/2019/11/risky-business-title-770x514.jpg" alt=""></div></div><div class="image-sub-caption">    <h3></h3>    <span class="author">By Brin-Jonathan Butler</span></div><h2 class="main-title"></h2><h3 class="intro-text"></h3><div class="content-text"><p><span style="font-weight: 400; font-size: 12.000000pt;">DEONTAY WILDER, BOXING’S MOST DANGEROUS AND VULNERABLE KO ARTIST, FACES LUIS ORTIZ IN A REMATCH THAT COULD (ONCE AGAIN) UPSET THE DELICATE BALANCE OF THE HEAVYWEIGHT DIVISION</span></p>
        <p><span style="font-weight: 400;"><strong><span style="font-size: 12.000000pt;">B</span></strong>ack in November 2018, in the lead-up to WBC heavyweight titleholder Deontay Wilder squaring off against lineal champion Tyson Fury at the Staples Center in Los Angeles, I flew down to Tuscaloosa, Alabama, to catch the last two days of sparring in Wilder’s training camp. There were three sparring partners, each specifically chosen for his Fury-like stature, and one of them even conspicuously formulated his style the best he could to create an ersatz karaoke of Fury’s uniquely awkward movements and tactics in the ring. Several journalists had flown in from the United Kingdom to cover Wilder-Fury. They all stood silently mystified at the back of the gym observing Wilder as he struggled to deal with the fairly pedestrian problems the sparring partners presented.</span></p>
        <p><span style="font-weight: 400;">In contrast to what we were looking at, Wilder’s trainer, Jay Deas, shouted enthusiastic encouragement at his fighter and smiled approvingly: “That’s it, D! Beautiful! Be first, D!” A sign hung above the ring celebrated “The Next Heavyweight Champion of the World, The Bronze Bomber!!” The “Next” had been crossed out with a sharpie, replaced with “NEW.” Another sign listed two columns of champions, divided by “Short Term CHAMPS” and “Long Term CHAMPS.” Leon Spinks, Trevor Berbick, Pinklon Thomas, Tony Tubbs, Frank Bruno and Bermane Stiverne comprised the former column; </span><span style="font-weight: 400;">Rocky Marciano, Muhammad Ali, George Foreman, Larry Holmes, Lennox Lewis and DEONTAY WILDER rounded out the latter. Wilder had held his championship for just under four years at that point after defeating Stiverne. </span></p>
        <p><div class="iframe-rwd"><iframe src="https://www.youtube.com/embed/Lg2uQ1uoBJU?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></p>
        <p><span style="font-weight: 400;">In between rounds, I approached the group of British journalists and whispered a question about what they made so far of watching the 40-0 American champion with 39 knockouts on his résumé spar. The most senior of the group turned to me with an almost embarrassed look on his face and confessed, “If this here is where Wilder’s at when he steps into the ring? Well, if Wilder can’t knock out Fury, he might easily lose every round with Fury boxing circles around him.”</span></p>
        <p><span style="font-weight: 400;">The bell rang and we stood and watched several more rounds until they had wrapped up sparring for the evening. Nothing I witnessed dissuaded me from accepting the British boxing writer’s assessment. Two glaring, parallel truths encompass watching Deontay Wilder work up close: If Wilder isn’t in possession of one of the most dangerous right hands in the history of boxing, he certainly wields the most formidable weapon today; secondly, he frequently demonstrates such surreal absence of coordination and technique while engaged in combat that you can’t shake the feeling you might be watching the most crude practitioner of the sweet science ever to hold a title. I simultaneously believe Wilder’s offensive gifts would pose a serious risk to any heavyweight champion who ever lived while also believing he engenders more than enough liabilities as a boxer to comfortably lose on even a mildly off night against virtually any elite heavyweight contender today: Andy Ruiz Jr., Anthony Joshua, Luis Ortiz, Fury, Aleksandr Usyk. </span></p>
        <blockquote><p><span style="font-weight: 400;">What makes the rematch between Wilder and Ortiz so compelling is not just what hangs in the balance for either fighter if they win, but how much both have to prove from their previous fight.</span></p></blockquote>
        <p><span style="font-weight: 400;">On December 1, 2018, at the Staples Center, according to many fight fans and writers, Wilder lived up to the U.K. writer’s prediction that night in Tuscaloosa. If Wilder didn’t lose every round he was unable to put Fury on the canvas, he certainly didn’t win even one of those rounds convincingly. The judges handed down a draw for the contest and Wilder retained his title to a roar of boos. </span></p>
        <p><span style="font-weight: 400;">This was the second fight in a row in which Wilder narrowly retained his title. Only nine months prior at Barclays Center in Brooklyn, New York, in the seventh round, if Luis Ortiz had 20 more seconds, he might well have finished off a badly rocked Wilder. Instead, Wilder overcame adversity to storm back <a href="https://www.ringtv.com/530356-deontay-wilder-survives-mid-fight-scare-knocks-luis-ortiz-10/" target="_blank" rel="noopener">with a vicious knockout victory in the 10th</a>. As with Fury, the drama and action between Wilder and Ortiz made it easily one of the most exciting fights of the year. </span></p>
        <p><div class="iframe-rwd"><iframe src="https://www.youtube.com/embed/KWNBVnscSho?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></p>
        <p><span style="font-weight: 400;">Now a November 23 date with “King Kong” Ortiz again stands in the way of a mega-rematch with Fury in February. But the formidable heavyweight landscape has shifted considerably over the last couple years. All kinds of vulnerabilities have been exposed among the titans in the division. Back on June 1, Joshua was on the losing end of the biggest heavyweight upset since Buster Douglas knocked out Mike Tyson. Like Tyson before him, Joshua’s aura of invincibility was obliterated by Ruiz, a vast underdog, who from the opening bell fought every second of the fight to win rather than survive against a champion of Joshua’s stature. The undefeated Fury <a href="https://www.ringtv.com/548927-tyson-fury-survives-two-knockdowns-at-hands-of-deontay-wilder-settles-for-controversial-draw/" target="_blank" rel="noopener">miraculously avoided being knocked out</a> by Wilder in one of the most dramatic moments in heavyweight championship history. Ortiz was likely one clean blow away from flattening Wilder. Hundreds of millions of dollars remain up for grabs with several permutations of megafights available if promoters can get their ducks lined up in a row. Tune-ups and marinating these megafights, as we’ve seen, offer all kinds of risks with this crop of titleholders. What looked like a golden age in the heavyweight division with three undefeated champions playing musical chairs to unify the title now threatens to be derailed by boxing politics before the most lucrative, landmark heavyweight fights of the decade take place. If Joshua loses again to Ruiz, he might well retire before even facing either Wilder or Fury in megafights. </span></p>
        <p><span style="font-weight: 400;">The Cuban “boogeyman” Ortiz, now in his 40s, undefeated prior to his loss to Wilder, has won three fights in a row leading up to the rematch at the MGM Grand Garden Arena in Las Vegas. Despite his age and previous issues failing drug tests, Ortiz proved he had the firepower and poise to become the first Cuban heavyweight champion in history with his previous match against Wilder. </span></p>
        <p><span style="font-weight: 400;">Wilder, an 11-year veteran of the sport, now 33 years of age, has successfully defended his title nine times. He has the opportunity to make a real statement in the division as the dominant heavyweight of his era if he can back up his prediction of an early knockout against Ortiz. Ortiz has the opportunity to avenge the only loss on his résumé and transform the heavyweight division nearly as much as Ruiz did against Joshua. </span></p>
        <p><div class="img_resizable"><img class="alignnone size-full wp-image-583579" src="http://www.ringtv.com/wp-content/uploads/2019/11/Wilder-vs-Ortiz-2-Press-Conference_11_23_2019_Presser_Nabeel-Ahmad-_-Premier-Boxing-Champions2.jpg" alt=""  sizes="(max-100vw, 1000px" /></div></p>
        <p><span style="font-weight: 400;">What makes the rematch between Wilder and Ortiz so compelling is not just what hangs in the balance for either fighter if they win, but how much both have to prove from their previous fight. Ortiz likely knows if he fails in this opportunity, another at this stage of his career is unlikely, which could mean we see him go for broke in an all-out assault early. Wilder’s ego and competitiveness might welcome such an approach, despite the precarious situation he overcame in the seventh round against Ortiz last time. But Wilder has now held a title for four years, and the defining fight of his career supplied Tyson Fury with one of the great comeback stories in the annals of boxing. To his credit, the Alabamian is fighting not just for massive paydays but also for his place in history; he is in his prime with the next two years poised to define his lasting legacy, and the risky Ortiz rematch stands in the way. </span></p>
        <p><span style="font-weight: 400;">Wilder has fought without a loss for 11 years, and his supporters point to his menacing 98 percent knockout-to-win ratio as evidence of being his era’s dominant heavyweight. If one scenario plays out, Ortiz might be the first step toward running the table with the likes of Ruiz, Joshua, Fury or the emerging Aleksandr Usyk. The other scenario might look something like undefeated heavyweight giant Michael Grant in 2000 before stepping into the ring with Lennox Lewis in Madison Square Garden for the IBF and WBC titles. Grant’s résumé included 31 victories and 22 knockouts, and his imposing stature and power had some voices in the sport suggesting he might assume the mantle of boxing’s next dominant champion. In two rounds, Lewis concluded any discussion of Grant being a generational talent. The same might happen for Wilder if he’s on the losing end of an iconic knockout loss at the hands of Joshua, Ruiz, Fury or Usyk. It almost happened against an aging Luis Ortiz. </span></p>
        <p><span style="font-weight: 400;">What makes Wilder the most exciting fighter in the sport to watch is how he bizarrely and simultaneously encompasses the potential for both extreme scenarios. With every signature fight of his career, just as much as every round of sparring in Tuscaloosa, a knockout never seems far away – one way or the other. </span></p>
        <div class="star-hr"><div class="img_resizable"><img src="https://www.ringtv.com/wp-content/themes/thering/images/stars.jpg" alt="" /></div></div>
        </div>
                
                <script type="text/javascript">
                    
                    window.onload = function() {
                        var wpFigures = document.getElementsByTagName("figure");
                        for (i=0; i<wpFigures.length;i++) {
                            currentImage = wpFigures[i].getElementsByTagName("img");
                            try {
                                currentWidth = currentImage[0].clientWidth;
                                if (currentWidth > 1) {
                                    wpFigures[i].style.width = currentWidth + 'px';
                                    console.log("updated", currentWidth, wpFigures[i].style.width);
                                }
                            }
                            catch(errMsg) {
                                console.log("unipress-error",errMsg);
                            }
                        }
                        
                        // Align left author images
                        var images = document.getElementsByTagName('img');
                        for (var imgIndex = 0; imgIndex < images.length; imgIndex++) {
                            var image =  images[imgIndex];
                            if (image.width <= 125) {
                                image.style.marginLeft = "10px";
                            }
                        }
                    };
                
                    </script>
                
                
            </body>
        </html>
""", baseURL: nil)
 */
        
    }
    
    @objc func updateCellHeight() {
        height = webView.scrollView.contentSize.height

        //heightConstraint.isActive = false
        heightConstraint = webView.heightAnchor.constraint(equalToConstant: height)
        heightConstraint.isActive = true

        self.tableView.reloadData()
        
        
//        self.asyncGetHTMLHeight(webView, completion: { newHeight in
//            self.webView.frame.size.height = newHeight
//            self.webView.removeConstraints(self.webView.constraints)
//           self.webView.heightAnchor.constraint(equalToConstant: newHeight).isActive = true
//            
//
//        })
        
    }
    
    func asyncGetHTMLHeight(_ webview: WKWebView, completion: @escaping (CGFloat) -> Void) {
        webview.evaluateJavaScript("Math.min(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight)") { (result, _) in
            guard let result = result as? CGFloat else {
                completion(0.0)
                return
            }
            completion(result)
        }
    }
    
    //MARK: - WKWebView
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        let cssString = "body { font-size: 50px; font-family: Arial}"
//        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
//        webView.evaluateJavaScript(jsString, completionHandler: nil)
        
        Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(updateCellHeight), userInfo: nil, repeats: false)
    }

    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        //        let js = "var meta = document.createElement('meta');meta.setAttribute('name', 'viewport');meta.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no');document.getElementsByTagName('head')[0].appendChild(meta);"
        //webView.evaluateJavaScript(js, completionHandler: nil)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
    
}
